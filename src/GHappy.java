public class GHappy {
/*
    4) String-3 > gHappy

    We'll say that a lowercase 'g' in a string is "happy" if there is another 'g' immediately to its
    left or right. Return true if all the g's in the given string are happy.

    gHappy("xxggxx") → true
    gHappy("xxgxx") → false
    gHappy("xxggyygxx") → false

    https://codingbat.com/prob/p198664
*/

    private boolean gHappy(String str) {
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == 'g'){
                if((i-1 < 0 || str.charAt(i-1) != 'g') && (i+1 >= str.length() || str.charAt(i+1) != 'g'))
                    return false;
            }
        }
        return true;
    }

    public void execute() {
        System.out.println("\n4) String-3 > gHappy\n");
        System.out.println("gHappy(\"xxggxx\")" + " -> " + gHappy("xxggxx"));
        System.out.println("gHappy(\"xxgxx\")" + " -> " + gHappy("xxgxx"));
        System.out.println("gHappy(\"xxggyygxx\")" + " -> " + gHappy("xxggyygxx"));
        System.out.println("\n========================================");
    }
}
