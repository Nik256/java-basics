public class CountTriple {
/*
    3) String-3 > countTriple

    We'll say that a "triple" in a string is a char appearing three times in a row. Return the
    number of triples in the given string. The triples may overlap.

    countTriple("abcXXXabc") → 1
    countTriple("xxxabyyyycd") → 3
    countTriple("a") → 0

    https://codingbat.com/prob/p195714
*/

    private int countTriple(String str) {
        int tripleCount = 0;
        int count = 0;
        for (int i = 2; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(i - 1) && str.charAt(i) == str.charAt(i - 2))
                tripleCount++;
        }
        return tripleCount;
    }

    public void execute() {
        System.out.println("\n3) String-3 > countTriple\n");
        System.out.println("countTriple(\"abcXXXabc\")" + " -> " + countTriple("abcXXXabc"));
        System.out.println("countTriple(\"xxxabyyyycd\")" + " -> " + countTriple("xxxabyyyycd"));
        System.out.println("countTriple(\"a\")" + " -> " + countTriple("a"));
        System.out.println("\n========================================");
    }
}
