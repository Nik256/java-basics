public class Main {
    public static void main(String[] args) {
/*
        1) String-3 > sumDigits

        Given a string, return the sum of the digits 0-9 that appear in the string, ignoring all other
        characters. Return 0 if there are no digits in the string. (Note: Character.isDigit(char) tests
        if a char is one of the chars '0', '1', .. '9'. Integer.parseInt(string) converts a string to an int.)

        sumDigits("aa1bc2d3") → 6
        sumDigits("aa11b33") → 8
        sumDigits("Chocolate") → 0

        https://codingbat.com/prob/p197890
*/
        SumDigits sumDigits = new SumDigits();
        sumDigits.execute();

/*
        2) String-3 > countYZ

        Given a string, count the number of words ending in 'y' or 'z' -- so the 'y' in "heavy" and
        the 'z' in "fez" count, but not the 'y' in "yellow" (not case sensitive). We'll say that a y or z
        is at the end of a word if there is not an alphabetic letter immediately following it. (Note:
        Character.isLetter(char) tests if a char is an alphabetic letter.)

        countYZ("fez day") → 2
        countYZ("day fez") → 2
        countYZ("day fyyyz") → 2

        https://codingbat.com/prob/p199171
*/
        CountYZ countYZ = new CountYZ();
        countYZ.execute();

/*
        3) String-3 > countTriple

        We'll say that a "triple" in a string is a char appearing three times in a row. Return the
        number of triples in the given string. The triples may overlap.

        countTriple("abcXXXabc") → 1
        countTriple("xxxabyyyycd") → 3
        countTriple("a") → 0

        https://codingbat.com/prob/p195714
*/
        CountTriple countTriple = new CountTriple();
        countTriple.execute();

/*
        4) String-3 > gHappy

        We'll say that a lowercase 'g' in a string is "happy" if there is another 'g' immediately to its
        left or right. Return true if all the g's in the given string are happy.

        gHappy("xxggxx") → true
        gHappy("xxgxx") → false
        gHappy("xxggyygxx") → false

        https://codingbat.com/prob/p198664
*/
        GHappy gHappy = new GHappy();
        gHappy.execute();

/*
        5) Array-3 > canBalance

        Given a non-empty array, return true if there is a place to split the array so that the sum
        of the numbers on one side is equal to the sum of the numbers on the other side.

        canBalance([1, 1, 1, 2, 1]) → true
        canBalance([2, 1, 1, 2, 1]) → false
        canBalance([10, 10]) → true

        https://codingbat.com/prob/p158767
*/
        CanBalance canBalance = new CanBalance();
        canBalance.execute();

/*
        6) Array-3 > seriesUp

        Given n>=0, create an array with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n}
        spaces added to show the grouping). Note that the length of the array will be 1 + 2 + 3 ... + n,
        which is known to sum to exactly n*(n + 1)/2.

        seriesUp(3) → [1, 1, 2, 1, 2, 3]
        seriesUp(4) → [1, 1, 2, 1, 2, 3, 1, 2, 3, 4]
        seriesUp(2) → [1, 1, 2]

        https://codingbat.com/prob/p104090
*/
        SeriesUp seriesUp = new SeriesUp();
        seriesUp.execute();

/*
        7) Array-3 > countClumps

        Say that a "clump" in an array is a series of 2 or more adjacent elements of the same
        value. Return the number of clumps in the given array.

        countClumps([1, 2, 2, 3, 4, 4]) → 2
        countClumps([1, 1, 2, 1, 1]) → 2
        countClumps([1, 1, 1, 1, 1]) → 1

        https://codingbat.com/prob/p193817
*/
        CountClumps countClumps = new CountClumps();
        countClumps.execute();

/*
        8) Array-3 > linearIn

        Given two arrays of ints sorted in increasing order, outer and inner, return true if all of
        the numbers in inner appear in outer. The best solution makes only a single "linear" pass of
        both arrays, taking advantage of the fact that both arrays are already in sorted order.

        linearIn([1, 2, 4, 6], [2, 4]) → true
        linearIn([1, 2, 4, 6], [2, 3, 4]) → false
        linearIn([1, 2, 4, 4, 6], [2, 4]) → true

        https://codingbat.com/prob/p134022
*/
        LinearIn linearIn = new LinearIn();
        linearIn.execute();
    }
}
