public class CountYZ {
/*
    2) String-3 > countYZ

    Given a string, count the number of words ending in 'y' or 'z' -- so the 'y' in "heavy" and
    the 'z' in "fez" count, but not the 'y' in "yellow" (not case sensitive). We'll say that a y or z
    is at the end of a word if there is not an alphabetic letter immediately following it. (Note:
    Character.isLetter(char) tests if a char is an alphabetic letter.)

    countYZ("fez day") → 2
    countYZ("day fez") → 2
    countYZ("day fyyyz") → 2

    https://codingbat.com/prob/p199171
*/

    private int countYZ(String str) {
        int count = 0;
        str = str.toLowerCase();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) == 'y' || str.charAt(i) == 'z') {
                if (i < length - 1 && !Character.isLetter(str.charAt(i + 1)))
                    count++;
                else if (i == length - 1)
                    count++;
            }
        }
        return count;
    }

    public void execute() {
        System.out.println("\n2) String-3 > countYZ\n");
        System.out.println("countYZ(\"fez day\")" + " -> " + countYZ("fez day"));
        System.out.println("countYZ(\"day fez\")" + " -> " + countYZ("day fez"));
        System.out.println("countYZ(\"day fyyyz\")" + " -> " + countYZ("day fyyyz"));
        System.out.println("\n========================================");
    }
}
