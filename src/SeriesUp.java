import java.util.Arrays;

public class SeriesUp {
/*
    6) Array-3 > seriesUp

    Given n>=0, create an array with the pattern {1,    1, 2,    1, 2, 3,   ... 1, 2, 3 .. n}
    spaces added to show the grouping). Note that the length of the array will be 1 + 2 + 3 ... + n,
    which is known to sum to exactly n*(n + 1)/2.

    seriesUp(3) → [1, 1, 2, 1, 2, 3]
    seriesUp(4) → [1, 1, 2, 1, 2, 3, 1, 2, 3, 4]
    seriesUp(2) → [1, 1, 2]

    https://codingbat.com/prob/p104090
*/

    private int[] seriesUp(int n) {
        int[] arr = new int[n * (n + 1) / 2];
        int k = 0;
        for (int i = 0; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                arr[k++] = j;
            }
        }
        return arr;
    }

    public void execute() {
        System.out.println("\n6) Array-3 > seriesUp\n");
        System.out.println("seriesUp(3)" + " -> " + Arrays.toString(seriesUp(3)));
        System.out.println("seriesUp(4)" + " -> " + Arrays.toString(seriesUp(4)));
        System.out.println("seriesUp(2)" + " -> " + Arrays.toString(seriesUp(2)));
        System.out.println("\n========================================");
    }
}
