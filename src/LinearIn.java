public class LinearIn {
/*
    8) Array-3 > linearIn

    Given two arrays of ints sorted in increasing order, outer and inner, return true if all of
    the numbers in inner appear in outer. The best solution makes only a single "linear" pass of
    both arrays, taking advantage of the fact that both arrays are already in sorted order.

    linearIn([1, 2, 4, 6], [2, 4]) → true
    linearIn([1, 2, 4, 6], [2, 3, 4]) → false
    linearIn([1, 2, 4, 4, 6], [2, 4]) → true

    https://codingbat.com/prob/p134022
*/

    private boolean linearIn(int[] outer, int[] inner) {
        if (inner.length == 0) {
            return true;
        }
        int k = 0;
        for (int i = 0; i < outer.length; i++) {
            if (outer[i] == inner[k]) {
                k++;
            } else if (outer[i] > inner[k]) {
                return false;
            }
            if (k == inner.length)
                return true;
        }
        return false;
    }

    public void execute() {
        System.out.println("\n8) Array-3 > linearIn\n");
        System.out.println("linearIn([1, 2, 4, 6], [2, 4])" + " -> " + linearIn(new int[]{1, 2, 4, 6}, new int[]{2, 4}));
        System.out.println("linearIn([1, 2, 4, 6], [2, 3, 4])" + " -> " + linearIn(new int[]{1, 2, 4, 6}, new int[]{2, 3, 4}));
        System.out.println("linearIn([1, 2, 4, 4, 6], [2, 4])" + " -> " + linearIn(new int[]{1, 2, 4, 4, 6}, new int[]{2, 4}));
        System.out.println("\n========================================");
    }
}
