public class CountClumps {
/*
    7) Array-3 > countClumps

    Say that a "clump" in an array is a series of 2 or more adjacent elements of the same
    value. Return the number of clumps in the given array.

    countClumps([1, 2, 2, 3, 4, 4]) → 2
    countClumps([1, 1, 2, 1, 1]) → 2
    countClumps([1, 1, 1, 1, 1]) → 1

    https://codingbat.com/prob/p193817
*/

    private int countClumps(int[] nums) {
        int count = 0;
        boolean flag = false;
        for (int i = 1; i < nums.length; i++) {
            if (!flag && nums[i] == nums[i - 1]) {
                count++;
                flag = true;
            }
            if (nums[i] != nums[i - 1])
                flag = false;
        }
        return count;
    }

    public void execute() {
        System.out.println("\n7) Array-3 > countClumps\n");
        System.out.println("countClumps([1, 2, 2, 3, 4, 4])" + " -> " + countClumps(new int[]{1, 2, 2, 3, 4, 4}));
        System.out.println("countClumps([1, 1, 2, 1, 1])" + " -> " + countClumps(new int[]{1, 1, 2, 1, 1}));
        System.out.println("countClumps([1, 1, 1, 1, 1])" + " -> " + countClumps(new int[]{1, 1, 1, 1, 1}));
        System.out.println("\n========================================");
    }
}
