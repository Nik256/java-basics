public class CanBalance {
/*
    5) Array-3 > canBalance

    Given a non-empty array, return true if there is a place to split the array so that the sum
    of the numbers on one side is equal to the sum of the numbers on the other side.

    canBalance([1, 1, 1, 2, 1]) → true
    canBalance([2, 1, 1, 2, 1]) → false
    canBalance([10, 10]) → true

    https://codingbat.com/prob/p158767
*/

    private boolean canBalance(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            int leftSum = 0;
            int rightSum = 0;
            for (int j = 0; j < nums.length; j++) {
                if (i < j)
                    leftSum += nums[j];
                else
                    rightSum += nums[j];
            }
            if (leftSum == rightSum)
                return true;
        }
        return false;
    }

    public void execute() {
        System.out.println("\n5) Array-3 > canBalance\n");
        System.out.println("canBalance([1, 1, 1, 2, 1])" + " -> " + canBalance(new int[]{1, 1, 1, 2, 1}));
        System.out.println("canBalance([2, 1, 1, 2, 1])" + " -> " + canBalance(new int[]{2, 1, 1, 2, 1}));
        System.out.println("canBalance([10, 10])" + " -> " + canBalance(new int[]{10, 10}));
        System.out.println("\n========================================");
    }
}
